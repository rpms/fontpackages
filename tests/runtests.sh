#!/bin/bash

echo "Test for fontpackages-filesystem"
rpm -qf `rpm -E %_fontbasedir`|grep fontpackages-filesystem
retval=$?
echo $retval
if [ $retval -ne 0 ]; then
        echo "FAILED: %_fontbasedir is not owned by fontpackages-filesystem"
else
        echo "PASSED: %_fontbasedir is owned by fontpackages-filesystem"
fi


echo "Test for fontpackages-devel"
rpm -E %_font_pkg
retval=$?
echo $retval
if [ $retval -ne 0 ]; then
        echo "FAILED: %_font_pkg macro is not available by fontpackages-devel"
else
        echo "PASSED: %_font_pkg macro is available by fontpackages-devel"
fi

echo "Test dependencies of font packages"
rpm -q --requires lohit-devanagari-fonts|grep fontpackages-filesystem
retval=$?
echo $retval
if [ $retval -ne 0 ]; then
        echo "FAILED: lohit-devanagari-fonts package does not require fontpackages-filesystem package"
else
        echo "PASSED: lohit-devanagari-fonts package does require fontpackages-filesystem package"
fi

